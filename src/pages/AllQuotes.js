import { useEffect } from "react";
import QuoteList from "../components/quotes/QuoteList";
import useHttp from "../hooks/use-http";
import { getAllQuotes } from "../lib/lib/api";
import LoadingSpinner from "../components/UI/LoadingSpinner";
import NoQuotesFoun from '../components/quotes/NoQuotesFound';
// const DUMMY_QUOTES = [
//   { id: "q1", author: "Adri", text: "Aprender react es divertido" },
//   { id: "q2", author: "Adriana", text: "Aprender react es bueno" },
// ];

const AllQuotes = () => {
  const {
    sendRequest,
    status,
    data: loadedQuotes,
    error,
  } = useHttp(getAllQuotes, true);

  useEffect(() => {
    sendRequest()
  },[sendRequest]);
  
  if(status === 'pending'){
    return(
        <div className="centered">
            <LoadingSpinner />
        </div>
    )
  }
  if(error) {
    <p className="centered focused">{error}</p>
  }

  if(status === 'completed' && (!loadedQuotes || loadedQuotes.length === 0)) {
    return <NoQuotesFoun  />
  }

  return <QuoteList quotes={loadedQuotes} />;
};
export default AllQuotes;
